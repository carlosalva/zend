<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module {

    protected $whitelist = array(
        'scn-social-auth-user/login',
        'scn-social-auth-user/login/twitter',
        'scn-social-auth-user/login/provider',
        'scn-social-auth-user/authenticate/provider',
        'scn-social-auth-hauth',
        'application',
        'registro'
    );

//    public function onBootstrap(MvcEvent $e)
//    {
//        $eventManager        = $e->getApplication()->getEventManager();
//        $moduleRouteListener = new ModuleRouteListener();
//        $moduleRouteListener->attach($eventManager);
//    }



    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getApplication();
        $sm = $app->getServiceManager();
        $allowedRoutes = $this->whitelist;
        $auth = $sm->get('zfcuser_auth_service');
        $app->getEventManager()->attach(
                \Zend\Mvc\MvcEvent::EVENT_ROUTE, function($e) use ($auth, $allowedRoutes) {
            $app = $e->getApplication();
            $routeMatch = $e->getRouteMatch();
            $routeName = $routeMatch->getMatchedRouteName();

            //die($routeName);
            if (!$auth->hasIdentity() && !in_array($routeName, $allowedRoutes)) {
                $response = $e->getResponse();
                $response->getHeaders()->addHeaderLine(
                        'Location', $e->getRouter()->assemble(
                                array(), array('name' => 'zfcuser/login')
                        )
                );
                $response->setStatusCode(302);
                return $response;
            }
        }, -100);
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
