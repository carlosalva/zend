<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class BaseController extends AbstractActionController {

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Sets the EntityManager
     *
     * @param EntityManager $em
     * @access protected
     * @return PostController
     */
    protected function setEntityManager(\Doctrine\ORM\EntityManager $em)
    {
        $this->entityManager = $em;
        return $this;
    }

    /**
     * Returns the EntityManager
     *
     * Fetches the EntityManager from ServiceLocator if it has not been initiated
     * and then returns it
     *
     * @access protected
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        if (null === $this->entityManager) {
            $this->setEntityManager($this->getServiceLocator()->get('Doctrine\ORM\EntityManager'));
        }
        return $this->entityManager;
    }

    public function enviarMail($para, $asunto, $cuerpo)
    {
        $message = new Message();
        $message->addTo($para)//matthew@zend.com
                ->addFrom('carlos_alva@usmp.pe')
                ->setSubject($asunto)
                ->setBody($cuerpo);

        // Setup SMTP transport using LOGIN authentication
        $transport = new SmtpTransport();
        $options = new SmtpOptions(array(
            'name' => 'smtp.mandrillapp.com',
            'host' => 'smtp.mandrillapp.com',
            'connection_class' => 'plain',
            'connection_config' => array(
                'username' => 'carlos_alva@usmp.pe',
                'password' => 'khoGJvTwxJQIrH_MCiKBcQ',
            )
        ));
        $transport->setOptions($options);
        $transport->send($message);
    }

}
