<?php

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use ZendService\Twitter\Twitter;
use DoctrineORMModule\Stdlib\Hydrator\DoctrineEntity;
use Application\Controller\BaseController;
use Application\Form\UserForm;
use Application\Entity\User;
use Zend\Crypt\Password\Bcrypt;

class ApplicationController extends BaseController {

    public function deleteAction()
    {
        $user = $this->getEntityManager()->getRepository('Application\Entity\User')->find($this->params('id'));

        if ($user) {
            $em = $this->getEntityManager();
            $em->remove($user);
            $em->flush();

            $this->flashMessenger()->addSuccessMessage('Usuario Eliminado');
        }

        return $this->redirect()->toRoute('application/default', array(
                        'controller' => 'application',
                        'action' => 'listado'
            ));
    }

    public function editUserAction($ruta = null)
    {
        $user = new User();
        $isAdd = true;
        if ($this->params('id') > 0) {
            $isAdd = false;
            $user = $this->getEntityManager()->getRepository('Application\Entity\User')->find($this->params('id'));
        }
        $form = new UserForm($this->getEntityManager());
        $form->setHydrator(new DoctrineEntity($this->getEntityManager(), 'Application\Entity\User'));
        $form->bind($user);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setInputFilter($form->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {

                return $this->saveUser($user, $isAdd, $ruta);
            }
        }
        return new ViewModel(array(
            'user' => $user,
            'form' => $form
        ));
    }

    private function saveUser($user, $isAdd, $ruta)
    {
        if ($isAdd) {
            $email = $this->params()->fromPost('email');
            $password = mt_rand(10000, 999999);
            //$this->enviarMail($email, "Su usuario se genero con exito", "Su clave de acceso  al sistema es :  $password");
            $passwordEncriptado = $this->encriptarPassword($password);
            $this->flashMessenger()->addSuccessMessage("Su clave de acceso  al sistema es :  $password");
            $user->setPassword($passwordEncriptado);
        }
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
        $this->flashMessenger()->addSuccessMessage('Usuario guardado');
        if (empty($ruta)) {
            return $this->redirect()->toRoute('application/default', array(
                        'controller' => 'application',
                        'action' => 'listado'
            ));
        }
        return $this->redirect()->toRoute($ruta);
    }

    private function encriptarPassword($password)
    {
        $options = $this->getServiceLocator()->get('zfcuser_module_options');
        $this->bcrypt = new Bcrypt();
        $this->bcrypt->setCost($options->getPasswordCost());
        $cryptPassword = $this->bcrypt->create($password);
        return $cryptPassword;
    }

    public function addUserAction()
    {

        return $this->editUserAction();
    }

    public function registerUserAction()
    {
        $ruta = 'zfcuser/login';
        return $this->editUserAction($ruta);
    }

    public function verTweetsAction()
    {
        $user = $this->getEntityManager()->getRepository('Application\Entity\User')->find($this->params('id'));

        if (empty($user)) {
            return $this->redirect()->toRoute('application', array('controller' => 'application',
                        'action' => 'listado'));
        }
        $twitter = new Twitter(array(
            'access_token' => array(// or use "accessToken" as the key; both work
                'token' => '1676480792-QdktTmjAnckM4DjAWuqRxSCi5C5sjSmqF5oV18j',
                'secret' => 'CgqydNNe5iaon7jRgoVwF3UNszLiMwVM0R5rmwpHChVnQ',
            ),
            'oauth_options' => array(// or use "oauthOptions" as the key; both work
                'consumerKey' => 'xoLPhoMxq3dMT6OoYAXvBDIoT',
                'consumerSecret' => '7HaS9kNySkt6iqX0QBc6RbZbDdToMgq9O9H5Q9NhrvMZvTkAMO',
            ),
            'http_client_options' => array(
                'adapter' => 'Zend\Http\Client\Adapter\Curl',
                'curloptions' => array(
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_SSL_VERIFYPEER => false,
                ),
            ),
        ));

        $response = $twitter->account->verifyCredentials();
        if (!$response->isSuccess()) {
            die('Falló las credenciales de twitter');
        }

        $response = $twitter->search->tweets('#' . $user->getUsername());

        $tweets = $response->toValue()->statuses;


        return new ViewModel(array('tweets' => $tweets));
    }

    public function listadoAction()
    {
        $entityManager = $this
                ->getServiceLocator()
                ->get('Doctrine\ORM\EntityManager');

        $usuarios = $entityManager->getRepository('Application\Entity\User')
                ->findAll();

        return new ViewModel(array(
            'usuarios' => $usuarios
        ));
    }

}
